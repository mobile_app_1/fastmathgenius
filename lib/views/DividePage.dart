import 'package:geniusmath/models/divide.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:geniusmath/timeOver.dart';
import 'package:get/get.dart';
import 'package:circular_countdown/circular_countdown.dart';

class DividePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final divideModel = Provider.of<Divide>(context);
    divideModel.init();
    final controller = TextEditingController();
    String userAnswer = '';

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              //Time & Score
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "Time:",
                        style: TextStyle(
                            fontFamily: 'Atma',
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: Colors.black),
                      ),
                      Column(children: [
                        TimeCircularCountdown(
                          unit: CountdownUnit.second,
                          countdownTotal: 45,
                          onFinished: () => gameOver(),
                        ),
                      ]),
                    ],
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Column(children: [
                    Text(
                      "Score:",
                      style: TextStyle(
                          fontFamily: 'Atma',
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.black),
                    ),
                    Column(children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20.0, horizontal: 20.0),
                        decoration: BoxDecoration(color: Colors.white),
                        child: Text(
                          "${divideModel.point}",
                          style: TextStyle(
                              fontFamily: 'Atma',
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: Colors.black),
                        ),
                      ),
                    ]),
                  ]),
                ],
              ),

              //Quiz
              Container(
                padding: EdgeInsets.fromLTRB(0, 50, 0, 0),
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey.shade400),
                    color: Colors.grey.shade400),
                width: 333,
                height: 198,
                child: Column(
                  children: [
                    Text(
                      "${divideModel.quiz}",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 80,
                        fontFamily: 'Atma',
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              //Answer
              Container(
                color: Colors.white,
                width: 300,
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    hintText: 'Input your answer',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    suffixIcon: userAnswer != null
                        ? IconButton(
                            onPressed: () => controller.clear(),
                            icon: Icon(Icons.clear),
                            color: Color.fromRGBO(240, 183, 117, 1),
                          )
                        : null,
                  ),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
              //Submit Button
              SizedBox(
                width: 258,
                height: 83,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll<Color>(
                          Color.fromRGBO(210, 85, 101, 1)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0)))),
                  onPressed: () {
                    userAnswer = controller.text;
                    var score = int?.parse(userAnswer);
                    print(userAnswer);
                    divideModel.checkAnswer(score);
                  },
                  child: Text(
                    'Submit',
                    style: TextStyle(
                        fontFamily: 'Atma',
                        fontSize: 40,
                        color: Color.fromRGBO(240, 183, 117, 1)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }

  gameOver() {
    Get.to(const timesOver());
  }
}
