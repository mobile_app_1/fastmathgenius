import 'package:flutter/material.dart';
import 'package:geniusmath/models/multiply.dart';
import 'package:geniusmath/models/minus.dart';
import 'package:provider/provider.dart';
import 'package:geniusmath/models/addition.dart';
import 'package:geniusmath/game.dart';

import 'models/divide.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => Addition(),
      ),
      ChangeNotifierProvider(
        create: (context) => Minus(),
      ),
      ChangeNotifierProvider(     
        create: (context) => Multiply(),
      ),
      ChangeNotifierProvider(
        create: (context) => Divide(),
      ),
    ],
    child: Game(),
  ));
}

