import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'mainPage.dart';

class score extends StatelessWidget {
  const score({super.key});


  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildBodyWidget() {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage("assets/images/bg.png"),
        fit: BoxFit.cover,
      ),
    ),
    child: Center(
      child: Column(
        children: <Widget>[
          TitleGame(),
          Padding(padding: EdgeInsets.symmetric(vertical: 40.0)),
          startButton(),
        ],
      ),
    ),
  );
}

Widget TitleGame() {
  return Container(
    padding: EdgeInsets.fromLTRB(0, 246, 0, 0),
    child: Column(
      children: [
        Text("score :" , textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold ,
              fontSize: 96 ,
              fontFamily: 'Atma',
              color: Color.fromRGBO(255, 253, 192, 1),
              shadows: <Shadow>[Shadow(
                offset: Offset(-5.0, 5.0),
                blurRadius: 3.0,
                color: Color.fromRGBO(240, 183, 117, 1),
                ),
              ],
          ),
        ),
        Text("5" , textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold ,
              fontSize: 80 ,
              fontFamily: 'Atma',
              color: Color.fromRGBO(255, 253, 192, 1),
              shadows: <Shadow>[Shadow(
                offset: Offset(-5.0, 5.0),
                blurRadius: 3.0,
                color: Color.fromRGBO(240, 183, 117, 1),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget startButton() {
  return Padding(
    padding: const EdgeInsets.all(4.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        SizedBox(
          width: 258,
          height: 83,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll<Color>(Color.fromRGBO(210, 85, 101, 1)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)
              ))
            ),
            onPressed: () {
              Get.to(MainPage());
            },
            child: Text('play again',
            style: TextStyle(
              fontFamily: 'Atma',
              fontSize: 40,
              color: Color.fromRGBO(240, 183, 117, 1))
              ,),
          ),
        )
      ],
    ),
  );
}