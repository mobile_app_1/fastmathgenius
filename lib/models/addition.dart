import 'dart:math';
import 'package:flutter/widgets.dart';
import 'dart:async';

import 'package:geniusmath/views/AdditonPage.dart';

class Addition with ChangeNotifier {
  int n1 = 0;
  int n2 = 0;
  int sum = 0;
  Random random = new Random();
  int point = 0;
  String quiz = "";
  int? userAnswer;

  void increaseValue() {
    point++;
    notifyListeners();
  }

  void init() {
    n1 = random.nextInt(10);
    n2 = random.nextInt(10);
    sum = n1 + n2;
    quiz = "${n1} + ${n2} =";
  }

  void genaratequiz() {
    n1 = random.nextInt(10);
    n2 = random.nextInt(10);
    sum = n1 + n2;
    quiz = "${n1} + ${n2} =";
    notifyListeners();
  }

  void resetGame() {
    n1 = random.nextInt(10);
    n2 = random.nextInt(10);
    sum = n1 + n2;
    quiz = "${n1} + ${n2} =";
    point = 0;
    notifyListeners();
  }

  void checkAnswer(int? userAnswer) {
    if (userAnswer == sum) {
      increaseValue();
      
    }
    notifyListeners();
    print(point);
    print(userAnswer);
  }

}
