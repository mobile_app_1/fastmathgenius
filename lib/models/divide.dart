import 'dart:math';
import 'package:flutter/widgets.dart';

class Divide with ChangeNotifier {
  int n1 = 0;
  int n2 = 0;
  int sum = 0;
  Random random = new Random();
  int point = 0;
  String quiz = "";
  int? userAnswer;
  int min = 5;
  int max = 10;

  void increaseValue() {
    point++;
    notifyListeners();
  }

  void init() {
    n1 = min + random.nextInt(max - min);
    n2 = random.nextInt(5)+1;
    sum = n1 ~/ n2;
    quiz = "${n1} ÷ ${n2} =";
  }

  void genaratequiz() {
    n1 = min + random.nextInt(max - min);
    n2 = random.nextInt(5)+1;
    sum = n1 ~/ n2;
    quiz = "${n1} ÷ ${n2} =";
    notifyListeners();
  }

  void resetGame() {
    n1 = min + random.nextInt(max - min);
    n2 = random.nextInt(5)+1;
    sum = n1 ~/ n2;
    quiz = "${n1} ÷ ${n2} =";
    point = 0;
    notifyListeners();
  }

  void checkAnswer(int? userAnswer) {
    if (userAnswer == sum) {
      increaseValue();
    }
    notifyListeners();
    print(point);
    print(userAnswer);
  }
}
