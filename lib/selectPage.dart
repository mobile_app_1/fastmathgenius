import 'package:flutter/material.dart';
import 'package:geniusmath/views/MinusPage.dart';
import 'package:geniusmath/views/AdditonPage.dart';
import 'package:geniusmath/views/MultiplyPage.dart';
import 'package:geniusmath/views/DividePage.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:geniusmath/models/addition.dart';
import 'package:geniusmath/models/minus.dart';
import 'package:geniusmath/models/multiply.dart';
import 'package:geniusmath/models/divide.dart';

class selectPage extends StatelessWidget {
  Widget build(BuildContext context) {
    final add = Provider.of<Addition>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: [
            buildBodyWidget(),
            //Plus
            Padding(
              padding: EdgeInsets.only(top: 200.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                        children: <Widget>[
                          SizedBox(
                            width: 258,
                            height: 75,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStatePropertyAll<Color>(
                                          Color.fromRGBO(210, 85, 101, 1)),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0)))),
                              onPressed: () {
                                add.resetGame();
                                Get.to(AdditionPage());
                              },
                              child: Text(
                                '+',
                                style: TextStyle(
                                    fontFamily: 'Atma',
                                    fontSize: 50,
                                    color: Color.fromRGBO(240, 183, 117, 1)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                  //Minus
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Consumer<Minus>(
                      builder: (context, minus, child) => Column(
                        children: <Widget>[
                          SizedBox(
                            width: 258,
                            height: 75,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStatePropertyAll<Color>(
                                      Color.fromRGBO(210, 85, 101, 1)),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(20.0)))),
                              onPressed: () {
                                minus.resetGame();
                                Get.to(MinusPage());
                              },
                              child: Text(
                                '-',
                                style: TextStyle(
                                    fontFamily: 'Atma',
                                    fontSize: 50,
                                    color: Color.fromRGBO(240, 183, 117, 1)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  //Multiply
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Consumer<Multiply>(
                      builder: (context, multiply, child) => Column(
                        children: <Widget>[
                          SizedBox(
                            width: 258,
                            height: 75,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStatePropertyAll<Color>(
                                      Color.fromRGBO(210, 85, 101, 1)),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(20.0)))),
                              onPressed: () {
                                multiply.resetGame();
                                Get.to(MultiplyPage());
                              },
                              child: Text(
                                '×',
                                style: TextStyle(
                                    fontFamily: 'Atma',
                                    fontSize: 50,
                                    color: Color.fromRGBO(240, 183, 117, 1)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  //Divide
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Consumer<Divide>(
                      builder: (context, divide, child) => Column(
                        children: <Widget>[
                          SizedBox(
                            width: 258,
                            height: 75,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStatePropertyAll<Color>(
                                      Color.fromRGBO(210, 85, 101, 1)),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(20.0)))),
                              onPressed: () {
                                divide.resetGame();
                                Get.to(DividePage());
                              },
                              child: Text(
                                '÷',
                                style: TextStyle(
                                    fontFamily: 'Atma',
                                    fontSize: 50,
                                    color: Color.fromRGBO(240, 183, 117, 1)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildBodyWidget() {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage("assets/images/bg.png"),
        fit: BoxFit.cover,
      ),
    ),
    child: Center(
      child: Column(
        children: <Widget>[
          TitleGame(),
        ],
      ),
    ),
  );
}

Widget TitleGame() {
  return Container(
    padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
    child: Column(
      children: [
        Text(
          "Select Level",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 80,
            fontFamily: 'Atma',
            color: Color.fromRGBO(255, 253, 192, 1),
            shadows: <Shadow>[
              Shadow(
                offset: Offset(-5.0, 5.0),
                blurRadius: 3.0,
                color: Color.fromRGBO(240, 183, 117, 1),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

