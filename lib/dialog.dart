import 'package:flutter/material.dart';
import '../const.dart';

class AlertMessageDialog extends StatelessWidget {
  final String message;
  final VoidCallback onTap;
  final icon;

  const AlertMessageDialog({
    Key? key,
    required this.message,
    required this.onTap,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.blue.shade200,
      content: Container(
        width: 150,
        height: 150,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          // the result
          Text(
            message,
            style: dialogTextStyle,
          ),
          // button to go to next question
          GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.all(2.0),
              decoration: BoxDecoration(
                  color: Colors.blue.shade100,
                  borderRadius: BorderRadius.circular(2)),
              child: Icon(icon, color: Colors.white, size: 30,),
            ),
          )
        ]),
      ),
    );
  }
}
